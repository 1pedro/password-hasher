#!/usr/bin/env python
#-*- encoding:utf-8 -*-
"""
    Author: Pedro Jefferson
    Email: pedrojefferson.developer@gmail.com
    Year: 2015
"""
    
    
from gi.repository import Gtk

class Janela():  #Main Window
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file('password_hasher.ui')
        self.builder.connect_signals(self)
        self.chk1 = self.builder.get_object('checkbutton1')
        self.entry = self.builder.get_object('entry1')
        self.label2 = self.builder.get_object('label2')
        self.window = self.builder.get_object('window1')
        self.window.show_all()

    def onButtonClicked(self, widget, data):  #Button Action
        if(self.chk1.get_active()):
            text = self.entry.get_text()
            text = hash(text.encode('base64'))
            self.label2.set_text('Hash: %i' %(text))
        else:
            text = self.entry.get_text()
            text = hash(text)
            self.label2.set_text('Hash: %i' %(text))
        

    def onDeleteWindow(self,widget, data):  #Window Action
        Gtk.main_quit()


if __name__ == "__main__":
    win = Janela()
    Gtk.main()
    
